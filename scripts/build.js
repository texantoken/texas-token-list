import { schema } from '@uniswap/token-lists'
import Ajv from 'ajv'
import addFormats from 'ajv-formats'

import * as fs from 'fs';

const chainfolder = 'chains'
const tokenfolder = 'tokens'
var tokens = []
var chains = []
var lists = []

fs.readdirSync(tokenfolder).forEach(file => {
	try {
		let token = JSON.parse(fs.readFileSync(tokenfolder + '/' + file))
		tokens.push(token)        
	}
	catch(e) {
		console.error('Token', file, 'ERROR', e)
		tokens.push({name: file})
	}
})

fs.readdirSync(chainfolder).forEach(file => {
	try {
		let chain = JSON.parse(fs.readFileSync(chainfolder + '/' + file))
		chains.push(chain)        
	}
	catch(e) {
		console.error('Chain', file, 'ERROR', e)
	}
})

chains.forEach( chain => {
	chain.tokens = tokens.filter( token => {
		return !token.chainId || (
			Array.isArray(token.chainId) 
			? token.chainId.indexOf(chain.chainId) != -1 
			: token.chainId == chain.chainId
		)
	}).map( token => {
		let copy = Object.assign({}, token)
		copy.chainId = chain.chainId
		return copy 
	})

	delete chain['explorer']
	delete chain['rpc']
	delete chain['symbol']
	delete chain['chainId']

	if(!chain.version.major) {
		chain.version.major = chain.tokens.length
	}

	if(!chain.version.minor) {
		chain.version.minor = Math.floor(Date.now() / (60 * 60 * 24 * 1000))
	}

	if(!chain.version.patch) {
		chain.version.patch = Math.floor(Date.now() / 1000)
	}

	chain.timestamp     = new Date().toISOString()
})

async function validate(chain) {
		const ajv = new Ajv({ allErrors: true, verbose: true })
		addFormats(ajv)
		const validator = ajv.compile(schema);
		const valid = validator(chain)

		if(valid) {
			return chain
		}
		if(validator.errors) {
			console.warn('List', chain.name, 'ERROR', validator.errors)
			return validator.errors.map(error => {
				delete error.data
				return error
			})
		}
}

chains.forEach( chain => {
	validate(chain)
		.then( list => {
			if(list.name){
				console.log(list.name + '.json')
				fs.writeFileSync('dist/' + list.name + '.json', JSON.stringify(chain, null, '\t'))
			}
		})
		.catch(console.error)
})



